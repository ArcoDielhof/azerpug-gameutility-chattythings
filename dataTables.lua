local GlobalAddonName, AGU = ...
_G[GlobalAddonName] = AGU

AGU.KeyPhrases =
{
        "Boost", "Boosting",
        "Selling Mythic", "Selling Heroic", "Selling Torghast", "Selling Castle", "Selling Keystone", "Selling M+", "Selling Sire", "Selling Denathrius", "Selling Nathria", "Selling Arena",
        "WTS Mythic", "WTS Heroic", "WTS Torghast", "WTS Castle", "WTS Keystone", "WTS M+", "WTS Sire", "WTS Denathrius", "WTS Nathria", "WTS Arena",
        "Gallywix", "Nova", "Icecrown", "Sylvanas", "RCU", "Oblivion", "Hydra", "Krakenboost", "SBS", "OOU", "Skycoach", "Twilight", "Exsorsus", "Titan", "NBC", "Triple DR",
}